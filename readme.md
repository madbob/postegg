## Postegg

Postegg è un semplice endpoint presso cui dirottare i POST di qualsiasi web form, affinché siano salvati pur senza doversi gestire un database.

È disponibile una istanza pubblica su http://postegg.madbob.org/

## Installazione

```
git clone https://gitlab.com/madbob/postegg.git
cd postegg
composer install
cp .env.example .env
(editare i parametri in .env)
php artisan migrate
npm run prod
```

Nel file `.env` vanno salvate anche le chiavi pubbliche e private per l'autenticazione su GitHub e GitLab.

## Licenza

Postegg è distribuito in licenza AGPLv3.

Copyright 2017 Roberto Guido <bob@linux.it>
