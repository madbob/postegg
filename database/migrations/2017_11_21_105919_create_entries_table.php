<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('key_id')->unsigned();
            $table->string('ip')->default('');
            $table->text('contents');
            $table->boolean('delivered')->default(false);
            $table->timestamps();

            $table->foreign('key_id')->references('id')->on('keys');
        });
    }

    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
