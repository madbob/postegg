<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    public function key()
    {
        return $this->belongsTo('App\Key');
    }

    public function getObjectContentsAttribute()
    {
        return json_decode($this->contents);
    }

    public function getPrettyContentsAttribute()
    {
        return json_encode($this->object_contents, JSON_PRETTY_PRINT);
    }
}
