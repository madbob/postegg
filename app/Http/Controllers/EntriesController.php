<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entry;

class EntriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function destroy(Request $request, $id)
    {
        $entry = Entry::find($id);
        if ($entry->key->user_id != $request->user()->id) {
            abort(403);
        }
        else {
            $entry->delete();
            return 'ok';
        }
    }
}
