<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;

use App\Key;
use App\Entry;

class SaveController extends Controller
{
    public function store(Request $request, $key)
    {
        $k = Key::where('key', $key)->first();
        if ($k == null) {
            abort(404);
        }

        $e = new Entry();

        $data = $request->all();
        $contents = json_encode($data);

        if (strlen($contents) > env('MAX_SUBMISSION_LENGTH')) {
            Log::error('Request too long');
            abort(413);
        }

        $e->contents = $contents;
        $e->key_id = $k->id;
        $e->ip = $request->ip();
        $e->save();

        if ($k->entries()->count() > env('MAX_SUBMISSION_COUNT')) {
            $k->entries()->offset(env('MAX_SUBMISSION_COUNT'))->delete();
        }

        return redirect()->away($k->redirect_url);
    }
}
