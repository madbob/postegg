<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;
use URL;
use Auth;
use LaravelGettext;

class HomeController extends Controller
{
    public function changeLang($locale=null)
    {
        LaravelGettext::setLocale($locale);
        return Redirect::to(URL::previous());
    }

    public function index()
    {
        $user = Auth::user();
        if ($user != null)
            return redirect()->route('keys.index');

        return view('home');
    }

    public function privacy()
    {
        return view('privacy');
    }
}
