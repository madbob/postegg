<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Socialite;

use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/keys';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider(Request $request, $driver)
    {
        return Socialite::driver($driver)->redirect();
    }

    public function handleProviderCallback(Request $request, $driver)
    {
        $user = Socialite::driver($driver)->user();
        $token = $user->getId();
        $field_name = $driver . '_token';

        $u = User::where($field_name, $token)->first();
        if ($u == null) {
            $u = new User();
            $u->$field_name = $token;
            $u->save();

            $u->createNewKey();
        }

        Auth::login($u);
        return redirect()->route('keys.index');
    }
}
