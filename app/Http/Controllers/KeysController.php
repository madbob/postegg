<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

use App\Key;

class KeysController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $keys = $request->user()->keys;
        return view('dashboard', ['keys' => $keys]);
    }

    public function store(Request $request)
    {
        $name = $request->input('name', '');
        $request->user()->createNewKey($name);
        return redirect()->route('keys.index');
    }

    public function update(Request $request, $id)
    {
        $key = Key::find($id);
        if ($key->user_id != $request->user()->id)
            return redirect()->route('keys.index');

        $key->name = $request->input('name', '');
        $key->redirect_url = $request->input('redirect_url', '');
        $key->save();
        return redirect()->route('keys.index');
    }

    public function show($id)
    {
        $key = Key::find($id);
        if ($key->user_id != $request->user()->id)
            return redirect()->route('keys.index');
        else
            return view('key', ['key' => $key]);
    }
}
