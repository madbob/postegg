<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

use App\Key;

class ReadController extends Controller
{
    public function readRss($private_key)
    {
        $key = Key::where('private_key', $private_key)->first();
        if ($key == null)
            abort(404);

        $entries = $key->entries()->take(15)->get();

        $feed = App::make("feed");
        $feed->title = 'Postegg - ' . $key->printable_name;
        $feed->description = 'Postegg - ' . $key->printable_name;
        $feed->link = $key->rss_feed;
        $feed->setDateFormat('datetime');

        if ($entries->count() > 0)
            $feed->pubdate = $entries[0]->created_at;
        else
            $feed->pubdate = '1970-01-01 00:00:00';

        foreach($entries as $e) {
            $feed->add($e->created_at, $e->ip, '', $e->created_at, $e->pretty_contents, $e->pretty_contents);
        }

        return $feed->render('rss');
    }

    public function readJson($private_key)
    {
        $key = Key::where('private_key', $private_key)->first();
        if ($key == null)
            abort(404);

        $ret = [];

        foreach($key->entries()->take(env('MAX_SUBMISSION_COUNT'))->get() as $e) {
            $node = (object) [
                'date' => $e->created_at,
                'ip' => $e->ip,
                'contents' => json_decode($e->contents)
            ];

            array_unshift($ret, $node);
        }

        return response()->json($ret);
    }

    public function readCsv($private_key)
    {
        $key = Key::where('private_key', $private_key)->first();
        if ($key == null)
            abort(404);

        $f = fopen('php://memory', 'w');

        foreach($key->entries()->take(env('MAX_SUBMISSION_COUNT'))->get() as $e) {
            $node = [
                $e->created_at,
                $e->ip,
                $e->contents
            ];

            fputcsv($f, $node);
        }

        $filename = $private_key . '.csv';
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        fpassthru($f);
    }
}
