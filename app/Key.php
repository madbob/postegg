<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    public function entries()
    {
        return $this->hasMany('App\Entry')->orderBy('created_at', 'desc');
    }

    public function getPrintableNameAttribute()
    {
        if (empty($this->name))
            return $this->key;
        else
            return $this->name;
    }

    public function getPostEndpointAttribute()
    {
        return url('/save/' . $this->key);
    }

    public function getRssFeedAttribute()
    {
        return route('read.rss', $this->private_key);
    }

    public function getJsonFeedAttribute()
    {
        return route('read.json', $this->private_key);
    }

    public function getCsvFeedAttribute()
    {
        return route('read.csv', $this->private_key);
    }
}
