<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Uuid;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password',
    ];

    /*
        https://laravel.io/forum/05-21-2014-how-to-disable-remember-token
    */

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    public function getRememberTokenName()
    {
        return null; // not supported
    }

    public function keys()
    {
        return $this->hasMany('App\Key')->orderBy('created_at', 'desc');
    }

    public function createNewKey($name = '')
    {
        $key = new Key();
        $key->user_id = $this->id;
        $key->name = $name;
        $key->key = Uuid::generate();
        $key->private_key = Uuid::generate();
        $key->save();
        return $key;
    }
}
