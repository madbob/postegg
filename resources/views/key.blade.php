<form method="POST" action="{{ route('keys.update', ['id' => $key->id]) }}">
    {!! csrf_field() !!}
    {{ method_field('PUT') }}

    <div class="row">
        <div class="col">
            <div class="form-group row">
                <label for="name" class="col-sm-3 col-form-label">{{ _i('Nome') }}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" value="{{ $key->name }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="redirect" class="col-sm-3 col-form-label">{{ _i('Redirect') }}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="redirect_url" value="{{ $key->redirect_url }}">
                    <small class="form-text text-muted">
                        {{ _i("URL dove redirigere l'utente dopo il salvataggio dei dati") }}
                    </small>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-3 col-form-label">{{ _i('POST endpoint') }}</label>
                <div class="col-sm-9">
                    <input type="text" readonly class="form-control" value="{{ $key->post_endpoint }}">
                    <small class="form-text text-muted">
                        {!! _i('Adopera questo URL come <code>action</code> del tuo form') !!}
                    </small>
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <label for="rss" class="col-sm-3 col-form-label">{{ _i('Feed RSS') }}</label>
                <div class="col-sm-9">
                    <input type="text" readonly class="form-control" value="{{ $key->rss_feed }}">
                    <small class="form-text text-muted">
                        {{ _i('Include le ultime 15 immissioni') }}
                    </small>
                </div>
            </div>
            <div class="form-group row">
                <label for="json" class="col-sm-3 col-form-label">{{ _i('Feed JSON') }}</label>
                <div class="col-sm-9">
                    <input type="text" readonly class="form-control" value="{{ $key->json_feed }}">
                    <small class="form-text text-muted">
                        {{ _i('Include tutte le immissioni (le ultime %s)', [env('MAX_SUBMISSION_COUNT')]) }}
                    </small>
                </div>
            </div>
            <div class="form-group row">
                <label for="csv" class="col-sm-3 col-form-label">{{ _i('Feed CSV') }}</label>
                <div class="col-sm-9">
                    <input type="text" readonly class="form-control" value="{{ $key->csv_feed }}">
                    <small class="form-text text-muted">
                        {{ _i('Include tutte le immissioni (le ultime %s)', [env('MAX_SUBMISSION_COUNT')]) }}
                    </small>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary pull-right">{{ _i('Salva') }}</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="row">
    <div class="col entries">
        @if($key->entries->count() == 0)
            <div class="alert alert-info">
                {{ _i('I dati saranno visualizzati qui, quando ce ne saranno.') }}
            </div>
        @else
            @foreach($key->entries as $entry)
                <hr>

                <div>
                    <a href="#" data-entry-id="{{ $entry->id }}" class="btn btn-danger pull-right entry-delete">
                        <i class="fa fa-remove"></i>
                    </a>

                    <h4>{{ $entry->created_at }} / {{ $entry->ip }}</h4>
                    <pre>{{ $entry->pretty_contents }}</pre>
                </div>

            @endforeach
        @endif
    </div>
</div>
