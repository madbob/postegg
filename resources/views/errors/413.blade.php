@extends('layouts.public')

@section('contents')

<section class="minor-header-wrapper text-center">
    <div>
        <img class="img-fluid" src="{{ url('/images/logo.png') }}">
    </div>
</section>

<section class="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2>413: troppi contenuti</h2>
                <p>
                    Ehi, hai immesso decisamente troppi contenuti nel form!
                </p>
                <p>
                    Torna indietro e prova ad essere più conciso ;-)
                </p>
            </div>
        </div>
    </div>
</section>

@endsection
