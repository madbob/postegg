@extends('layouts.public')

@section('contents')

<section class="minor-header-wrapper text-center">
    <div>
        <img class="img-fluid" src="{{ url('/images/logo.png') }}">
    </div>
</section>

<section class="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2>404: chiave non trovata</h2>
                <p>
                    Sembra che si sia verificato un problema con la tua richiesta...
                </p>
                <p>
                    Torna indietro, e contatta l'amministratore del sito su cui stavi compilando il form.
                </p>
            </div>
        </div>
    </div>
</section>

@endsection
