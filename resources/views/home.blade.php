@extends('layouts.public')

@section('contents')

<section class="header-wrapper text-center">
    <div>
        <img class="img-fluid" src="{{ url('/images/logo.png') }}">
        <h3>{{ _i('Raccogli dati dai tuoi utenti') }}<br>{{ _i('senza server e senza pensieri.') }}</h3>
        <br><br>
        <a href="#usage-wrapper" class="btn btn-success btn-lg">{{ _i('Provalo!') }}</a>
    </div>
</section>

<section class="features-wrapper">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center"><i class="fa fa-clipboard fa-3x" aria-hidden="true"></i></h4>
                            <p class="card-text">{{ _i('Copia e incolla un URL nella tua pagina web, e sei subito operativo.') }}</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center"><i class="fa fa-bullhorn fa-3x" aria-hidden="true"></i></h4>
                            <p class="card-text">{{ _i('RSS, JSON e CSV: molteplici canali di notifica dei dati caricati.') }}</p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center"><i class="fa fa-server fa-3x" aria-hidden="true"></i></h4>
                            <p class="card-text">{{ _i('Usalo gratuitamente, oppure scaricalo ed installalo su un tuo server personale.') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- FAQ -->

<section class="questions-wrapper">
    <div class="container">
        <div class="row">
            <div class="col lead">
                <p>
                    {!! _i("Con <strong>Postegg</strong> puoi raccogliere i dati immessi dai tuoi utenti in qualsiasi form online, senza doverli direttamente processare con una applicazione o salvare su un database. Puoi usarlo per un form di contatto, un sondaggio o la registrazione ad una newsletter, su un qualsiasi sito statico in HTML.") !!}
                </p>
                <p>
                    {!! _i("Per usarlo, basta registrarsi e definire alcuni parametri per ottenere una API key da usare direttamente nella <code>action</code> dei tuoi form, ad esempio:") !!}
                </p>
                <p>
                    <pre>&lt;form method="POST" action="{{ env('APP_URL') }}/save/{{ _i('LA_TUA_API_KEY') }}"&gt;
    &lt;input type="text" name="name" placeholder="{{ _i('Il Tuo Nome') }}"&gt;&lt;br&gt;
    &lt;input type="email" name="email" placeholder="{{ _i('La Tua EMail') }}"&gt;&lt;br&gt;
    &lt;button type="submit">{{ _i('Invia') }}&lt;/button&gt;
&lt;/form&gt;</pre>
                </p>
                <p>
                    {{ _i("I dati raccolti saranno esposti per mezzo di un feed RSS ed una API JSON, accessibili con un'altra API key, privata.") }}
                </p>
            </div>
        </div>
    </div>
</section>

<!-- UTILIZZO -->

<section class="usage-wrapper" id="usage-wrapper">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-left">{{ _i('Accedi...') }}</h4>
                            <p class="card-text">
                                {{ _i('Usa direttamente questo servizio.') }}
                            </p>
                            <p>
                                {{ _i('Attenzione: questa istanza, pubblica e gratuita, ha alcune limitazioni:') }}
                            </p>
                            <ul>
                                <li>{{ _i('vengono conservate al più %d immissioni per account. Oltre questo limite, le più vecchie vengono cancellate', [env('MAX_SUBMISSION_COUNT')]) }}</li>
                                <li>{{ _i('non vengono salvate immissioni con più di %d caratteri', [env('MAX_SUBMISSION_LENGTH')]) }}</li>
                                <li>{{ _i('non vengono salvate più di 10 immissioni al minuto') }}</li>
                            </ul>
                        </div>
                        <div class="card-footer text-center">
                            <span class="align-middle">
                                @if(!empty(env('GITHUB_CLIENT_ID')))
                                    <a class="btn btn-success" href="{{ url('login/github') }}">{{ _i('Login con GitHub') }}</a>
                                @endif
                                @if(!empty(env('GITLAB_KEY')))
                                    <a class="btn btn-success" href="{{ url('login/gitlab') }}">{{ _i('Login con GitLab') }}</a>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-right">{{ _i('... oppure scarica ed installa') }}</h4>
                            <p class="card-text">
                                {{ _i("Per una gestione totalmente personalizzata dell'applicazione e dei dati raccolti, scarica gratuitamente Postegg ed installalo sul tuo server o sul tuo spazio hosting.") }}
                            </p>
                        </div>
                        <div class="card-footer text-center">
                            <span class="align-middle">
                                <a class="btn btn-success" href="https://gitlab.com/madbob/postegg">{{ _i('Vedi il Codice') }}</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
