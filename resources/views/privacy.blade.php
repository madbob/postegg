@extends('layouts.public')

@section('contents')

<section class="minor-header-wrapper text-center">
    <div>
        <img class="img-fluid" src="{{ url('/images/logo.png') }}">
    </div>
</section>

<section class="privacy-wrapper">
    <div class="container">
        <div class="row">
            <div class="col">
                <p>
                    {{ _i('I dati non vengono condivisi con nessuno esterno alla piattaforma, se non in forma aggregata e non riconducibile a nessuna persona.') }}
                </p>
                <p>
                    {{ _i("Gli utenti registrati possono accedere alle informazioni di loro pertinenza qui salvate da soggetti terzi per mezzo del servizio pubblico, compresi la data di salvataggio e l'indirizzo IP.") }}
                </p>
                <p>
                    {{ _i('Vengono usati solo cookie "tecnici" di autenticazione.') }}
                </p>
                <p>
                    {!! _i('Il server sta in Germania, presso <a href="https://www.hetzner.com/">questo provider qui</a>.') !!}
                </p>
                <p>
                    {{ _i("Non sono responsabile per nessun dato o informazione sulla piattaforma. Né tantomeno della loro perdita, modifica, cancellazione, e per qualsiasi cosa possa succedere a loro e a causa del loro utilizzo.") }}
                </p>
                <p>
                    {!! _i('Per eliminare un account, e tutti i dati relativi, mandatemi una mail all\'indirizzo <a href="mailto:info@madbob.org">info@madbob.org</a>. Il tutto verrà cancellato entro 12 mesi.') !!}
                </p>
            </div>
        </div>
    </div>
</section>

@endsection
