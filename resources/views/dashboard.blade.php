@extends('layouts.public')

@section('contents')

<section class="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                    @foreach($keys as $index => $key)
                        <a class="nav-link {{ $index == 0 ? 'active' : '' }}" data-toggle="pill" href="#key_{{ $key->id }}" role="tab">{{ $key->printable_name }}</a>
                    @endforeach
                    <a href="#" class="nav-link" data-toggle="modal" data-target="#newKey">{{ _i('Crea Nuova Chiave') }}</a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content">
                    @foreach($keys as $index => $key)
                        <div class="tab-pane fade {{ $index == 0 ? 'show active' : '' }}" id="key_{{ $key->id }}" role="tabpanel">
                            @include('key', ['key' => $key])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="newKey" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ _i('Crea Nuova Chiave') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ _i('Annulla') }}">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ url('keys') }}">
                <div class="modal-body">
                    {!! csrf_field() !!}

                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">{{ _i('Nome') }}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i('Annulla') }}</button>
                    <button type="submit" class="btn btn-primary">{{ _i('Salva') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
