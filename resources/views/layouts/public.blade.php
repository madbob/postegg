<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ _i('Raccogli dati dai tuoi utenti con Postegg') }}</title>

        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div>
            @yield('contents')

            <div id="lang_chooser">
                <a href="{{ url('lang/it_IT') }}"><img src="{{ url('images/lang_it.png') }}"></a>
                <a href="{{ url('lang/en_US') }}"><img src="{{ url('images/lang_en.png') }}"></a>
            </div>

            <section class="contacts-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h3>Postegg</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col links">
                            <div class="row">
                                <div class="col-1">
                                    <p><i class="fa fa-envelope" aria-hidden="true"></i></p>
                                </div>
                                <div class="col-11">
                                    <p><a href="mailto:info@madbob.org">info@madbob.org</a></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-1">
                                    <p><i class="fa fa-gitlab" aria-hidden="true"></i></p>
                                </div>
                                <div class="col-11">
                                    <p><a href="https://gitlab.com/madbob/postegg">gitlab.com/madbob/postegg</a></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-1">
                                    <p><i class="fa fa-cogs" aria-hidden="true"></i></p>
                                </div>
                                <div class="col-11">
                                    <p>powered by <a href="http://madbob.org/"><img src="{{ url('/images/mad.png') }}"></a></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-1">
                                    <p><i class="fa fa-eye" aria-hidden="true"></i></p>
                                </div>
                                <div class="col-11">
                                    <p><a href="{{ url('/privacy') }}">{{ _i('Privacy') }}</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <p>{{ _i('Sostieni questo progetto, con una donazione PayPal') }}</p>
                            <a href="https://www.paypal.me/m4db0b">
                                <img src="/images/paypal.png">
                            </a>
                            <p>{{ _i('o con una sottoscrizione su Patreon') }}</p>
                            <a href="https://www.patreon.com/madbob">
                                <img src="/images/become_a_patron_button.png">
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
