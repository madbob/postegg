
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');

$('.entry-delete').click(function(e) {
    e.preventDefault();

    var button = $(this);
    button.addClass('disabled');

    if (confirm('Sei sicuro di voler eliminare questo elemento?')) {
        var id = button.attr('data-entry-id');
        var entry = button.closest('div');

        $.ajax({
            url: '/entries/' + id,
            method: 'DELETE',
            data: {
                _token: window.axios.defaults.headers.common['X-CSRF-TOKEN']
            },
            success: function() {
                entry.remove();
            },
            error: function() {
                button.removeClass('disabled');
            }
        });
    }
    else {
        button.removeClass('disabled');
    }
});
