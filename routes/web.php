<?php

Route::get('login', function() {
    return redirect()->route('home');
})->name('login');

Route::get('/', 'HomeController@index')->name('home');

Route::get('lang/{locale?}', [
    'as' => 'home',
    'uses' => 'HomeController@changeLang'
]);

Route::get('/privacy', 'HomeController@privacy')->name('privacy');

Route::get('login/{driver}', 'LoginController@redirectToProvider');
Route::get('login/{driver}/callback', 'LoginController@handleProviderCallback');
Route::get('logout', 'LoginController@logout');

Route::post('save/{key}', 'SaveController@store');
Route::get('read/{private_key}/rss', 'ReadController@readRss')->name('read.rss');
Route::get('read/{private_key}/json', 'ReadController@readJson')->name('read.json');
Route::get('read/{private_key}/csv', 'ReadController@readCsv')->name('read.csv');

Route::resource('keys', 'KeysController');
Route::resource('entries', 'EntriesController');
